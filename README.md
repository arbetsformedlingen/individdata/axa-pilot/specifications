# Specifications

This repo contains the OpenAPI specification for Mitt inskrivningsintyg.

## CI

Important files in this repo are linted to ensure high quality and correctness.

### Files matching glob `./mitt-inskrivningsintyg/**/*.yaml`

These files are linted with [vacuum][vacuum]. The recommended part of the OAS specifciation from Spectral is used in combination with [all rules from OWASP][owasp].
The configuration can be found in `rulesets/openapi-ruleset.yaml`.

More information about [the rules][rules].

### Files matching glob `./**/*.schema.json`

These files are linted with [ajv][ajv]. The files are expected to comply
with the [2020-12 draft specification][draft] for JSON Schema.

[vacuum]: https://quobix.com/vacuum/
[owasp]: https://quobix.com/vacuum/rules/owasp/
[rules]: https://quobix.com/vacuum/rules/
[ajv]: https://ajv.js.org/
[draft]: https://json-schema.org/draft/2020-12/release-notes
